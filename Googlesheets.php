<?php

class  Googlesheets extends Tools_Plugins_Abstract {
	protected function _init() {
		// initialize layout
		$this->_layout = new Zend_Layout();
		$this->_layout->setLayoutPath( Zend_Layout::getMvcInstance()->getLayoutPath() );

		// set proper view script pathes
		if ( ( $scriptPaths = Zend_Layout::getMvcInstance()->getView()->getScriptPaths() ) !== false ) {
			$this->_view->setScriptPath( $scriptPaths );
		}
		$this->_view->addScriptPath( __DIR__ . '/system/views/' );

		// initialize helpers
		$this->_configHelper = Zend_Controller_Action_HelperBroker::getStaticHelper( 'config' );
	}

	public function gdataAction() {
		$containerName = filter_var( $this->_request->getParam( 'container' ), FILTER_SANITIZE_STRING );
		$pageId        = filter_var( $this->_request->getParam( 'pageId' ), FILTER_SANITIZE_NUMBER_INT );
		if ( $pageId ) {
			$containerType = Application_Model_Models_Container::TYPE_REGULARCONTENT;
		} else {
			$containerType = Application_Model_Models_Container::TYPE_STATICCONTENT;
		}
		$container = Application_Model_Mappers_ContainerMapper::getInstance()->findByName(
			$containerName,
			$pageId,
			$containerType
		);
		$content   = '';
		if ( $container instanceof Application_Model_Models_Container ) {
			$content = explode( ':', $container->getContent() );
		}
		$type = '';
		if ( ! empty( $content[0] ) ) {
			if ( $content[0] !== 'gcharts' ) {
				$type = $content[0];
			} else {
				$type = $content[0] . '-' . $content[2];
				unset( $content[2] );
				$content = array_values( $content );
			}
		}

		$this->_view->type          = $type;
		$this->_view->name          = isset( $content[1] ) ? $content[1] : '';
		$this->_view->width         = isset( $content[2] ) ? $content[2] : '';
		$this->_view->height        = isset( $content[3] ) ? $content[3] : '';
		$this->_view->key           = isset( $content[4] ) ? $content[4] : '';
		$this->_view->select        = isset( $content[5] ) ? str_replace("SELECT ", "", $content[5]) : '';
		$this->_view->pageId        = $pageId;
		$this->_view->containerName = $containerName;

		// Render
		$this->_layout->content = $this->_view->render( 'config.phtml' );
		echo $this->_layout->render();
	}

}

