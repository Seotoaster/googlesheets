Googlesheets plugin

{$googlesheets:name[:static]}

Widget can be added in container or in template.

Options:
name - name of the widget.
static - means that this widget is not depended from a current page.

Examples:
{$googlesheets:name}
{$googlesheets:name:static}
