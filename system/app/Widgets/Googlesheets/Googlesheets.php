<?php

class Widgets_Googlesheets_Googlesheets extends Widgets_Abstract {

	/**
	 * Cache flag, shows whether this widget cached
	 *
	 * @var bool
	 */
	protected $_cacheable = false;

	/**
	 * Website url
	 *
	 * @var string
	 */
	protected $_websiteUrl = '';


	protected function _init() {
		$this->_view = new Zend_View( array(
				'scriptPath' => __DIR__ . '/views'
			)
		);

		$containerName = 'gd_' . substr( md5( $this->_options[0] ), 0, 24 );
		$pageId        = ( end( $this->_options ) == 'static' ) ? 0 : $this->_toasterOptions['id'];
		$containerType = ( $pageId ) ? Application_Model_Models_Container::TYPE_REGULARCONTENT : Application_Model_Models_Container::TYPE_STATICCONTENT;
		$container     = Application_Model_Mappers_ContainerMapper::getInstance()->findByName( $containerName,
			$pageId,
			$containerType );

		// assign view variables
		$this->_view->container = $containerName;
		$this->_view->pageId    = $pageId;
		$content                = '';
		if ( $container instanceof Application_Model_Models_Container ) {
			$content = $container->getContent();
			$content = sprintf( '{$%s}', $content );
		}
		$this->_view->content = $content;
	}

	protected function _load() {
		$this->_view->websiteUrl = Zend_Controller_Action_HelperBroker::getStaticHelper( 'website' )->getUrl();

		return $this->_view->render( 'gdata.phtml' );

	}

}