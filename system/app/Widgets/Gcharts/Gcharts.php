<?php

class Widgets_Gcharts_Gcharts extends Widgets_Abstract {
	protected $_cacheable = false;

	protected function  _init() {
		$this->_view = new Zend_View( array(
				'scriptPath' => __DIR__ . '/views'
			)
		);
		$this->_view->addHelperPath( 'ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper' );
	}

	protected function _load() {
		return $this->_renderGcharts();
	}

	protected function _renderGcharts() {
		$this->_view->name           = filter_var( $this->_options[0], FILTER_SANITIZE_STRING );
		$this->_view->type           = ucfirst( filter_var( $this->_options[1], FILTER_SANITIZE_STRING ) );
		$this->_view->width          = (int) empty( $this->_options[2] ) ? 300 : $this->_options[2];
		$this->_view->height         = (int) empty( $this->_options[3] ) ? 300 : $this->_options[3];
		$this->_view->chartTableLink = 'http://spreadsheets.google.com/tq?key=' . filter_var( $this->_options[4],
				FILTER_SANITIZE_STRING ) . '&pub=1';
		if ( ! empty( $this->_options[5] ) ) {
			$this->_view->chartSql = html_entity_decode( filter_var( $this->_options[5], FILTER_SANITIZE_STRING ) );
		} else {
			$this->_view->chartSql = '';
		}
		$this->_view->hash = substr( md5( $this->_view->name ), 0, 3 );

		if ( ! Zend_Registry::isRegistered( 'jsapi' ) ) {
			$this->_view->jsapi = '<script type="text/javascript" src="https://www.google.com/jsapi"></script>';
			Zend_Registry::set( 'jsapi', $this->_view->jsapi );
		}

		return $this->_view->render( 'gcharts.phtml' );
	}
}
