<?php

/**
 * Google sheets API
 * @author Vitaly Vyrodov <vitaly.vyrodov@gmail.com>
 */
class Api_Googlesheets_Googlesheets extends Api_Service_Abstract {

	protected $_accessList = array(
		Tools_Security_Acl::ROLE_USER       => array( 'allow' => array( 'get', 'post', 'put', 'delete' ) ),
		Tools_Security_Acl::ROLE_SUPERADMIN => array( 'allow' => array( 'get', 'post', 'put', 'delete' ) ),
		Tools_Security_Acl::ROLE_ADMIN      => array( 'allow' => array( 'get', 'post', 'put', 'delete' ) )
	);

	public function postAction() {
		$containerName = filter_var( $this->_request->getParam( 'container' ), FILTER_SANITIZE_STRING );
		$pageId        = filter_var( $this->_request->getParam( 'pageId' ), FILTER_SANITIZE_NUMBER_INT );
		$containerType = ( $pageId ) ? Application_Model_Models_Container::TYPE_REGULARCONTENT : Application_Model_Models_Container::TYPE_STATICCONTENT;

		if ( $pageId == 0 ) {
			$pageId = null;
		}

		$type   = filter_var( $this->_request->getParam( 'type' ), FILTER_SANITIZE_STRING );
		$name   = filter_var( $this->_request->getParam( 'name' ), FILTER_SANITIZE_STRING );
		$key    = filter_var( $this->_request->getParam( 'key' ), FILTER_SANITIZE_STRING );
		$width  = filter_var( $this->_request->getParam( 'width' ), FILTER_SANITIZE_NUMBER_INT );
		$height = filter_var( $this->_request->getParam( 'height' ), FILTER_SANITIZE_NUMBER_INT );
		$select = filter_var( $this->_request->getParam( 'select' ), FILTER_SANITIZE_STRING );
		$select = $select ? "SELECT " . strtoupper($select) : "";
		if ( false !== strpos( $type, 'gcharts' ) ) {
			$chartsTypes = explode( '-', $type );
			$type        = $chartsTypes[0];
			$chartType   = $chartsTypes[1];
			$content     = $type . ':' . $name . ':' . $chartType . ':' . $width . ':' . $height . ':' . $key . ":" . $select;
		} else {
			$content     = $type . ':' . $name . ':' . $width . ':' . $height . ':' . $key . ":" . $select;
		}

		$mapper    = Application_Model_Mappers_ContainerMapper::getInstance();
		$container = $mapper->findByName( $containerName, $pageId, $containerType );
		if ( ! $container instanceof Application_Model_Models_Container ) {
			$container = new Application_Model_Models_Container();
			$container->setPageId( $pageId )
			          ->setContainerType( $containerType )
			          ->setName( $containerName );
		}
		$container->setContent( $content );

		try {
			return array( 'error' => false, 'responseText' => $mapper->save( $container ) );
		} catch ( Exception $e ) {
			return $this->_error( $e->getMessage() );
		}
	}

	public function getAction() {
	}

	public function putAction() {
	}

	public function deleteAction() {
		parse_str( $this->_request->getRawBody(), $data );
		$name   = filter_var( $data['containerName'], FILTER_SANITIZE_STRING );
		$pageId = filter_var( $data['pageId'], FILTER_SANITIZE_NUMBER_INT );
		$type   = ( $pageId ) ? Application_Model_Models_Container::TYPE_REGULARCONTENT : Application_Model_Models_Container::TYPE_STATICCONTENT;

		if ( $pageId == 0 ) {
			$pageId = null;
		}

		$mapper    = Application_Model_Mappers_ContainerMapper::getInstance();
		$container = $mapper->findByName( $name, $pageId, $type );
		if ( ! $container instanceof Application_Model_Models_Container ) {
			return array( 'error' => false );
		}
		try {
			return array( 'error' => false, 'responseText' => $mapper->delete( $container ) );
		} catch ( Exception $e ) {
			return $this->_error( $e->getMessage() );
		}
	}


}